@extends('layouts.app')

@section('content')

    <h2>Ma liste d'étudiants</h2>

    <ul>
        @forelse( $students as $student )

            @if( $loop->first  )
                <li>{{ $student }}, numéro UNO</li>
            @elseif( $loop->last )
                <li>{{ $student }}, dernier sorry</li>
            @elseif( $loop->index === 1 )
                <li>{{ $student }}, numéro UNO</li>
            @else
                <li>{{ $student }}</li>
            @endif

        @empty
        <li>Aucun élève</li>
        @endforelse
    </ul>


@endsection
