@extends('layouts.app')

@section('content')
<ul>
    @foreach ($articles as $article)
    {{dd($article)}}
        <li>titre :{{$article->title}}</li>
        <li><a href="{{route('blog.articles.index',$article->id)}}" ><img src="{{$article->image_url}}"></a></li>
        <li>Desc: {{$article->content}}</li>
        <li>Date: {{$article->published_at}}</li>
        <li>Categorie: {{$article->categories}}</li>
        <li>Auteur: {{$article->author->name }}</li>
        <br>
    @endforeach
</ul>
@endsection
