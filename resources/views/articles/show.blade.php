@extends('layouts.app')

@section('content')
<ul>
        <li>titre :{{$article->title}}</li>
        <li><img src="{{$article->image_url}}"></li>
        <li>Desc: {{$article->content}}</li>
        <li>Date: {{$article->published_at}}</li>
        <li>Auteur: {{$article->author_id}}</li>
        <br>
</ul>

<a href="{{route('blog.articles.index')}}">retour</a>
@endsection
