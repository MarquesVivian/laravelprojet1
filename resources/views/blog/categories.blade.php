@extends('layouts.app')


@section('content')
    <ul>
        @foreach ($categories as $category)
            <li><a href="{{route('blog.categories.show',$category->id)}}">{{$category->name}} ({{$category->article_count}})</a>
            </li>
        @endforeach

    </ul>
@endsection