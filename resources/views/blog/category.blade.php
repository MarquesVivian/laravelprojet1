@extends('layouts.app')


@section('content')
    <ul>
        <h2><li>Catégorie : {{$categorie->name}}</li></h2>
        <br>
        @foreach ($categorie->article as $article)
            <li>titre :{{$article->title}}</li>
            <li><img src="{{$article->image_url}}"></li>
            <li>Desc: {{$article->content}}</li>
            <li>Date: {{$article->published_at}}</li>
            <li>Auteur: {{$article->author_id}}</li>
            <br>
        @endforeach
    </ul>
@endsection