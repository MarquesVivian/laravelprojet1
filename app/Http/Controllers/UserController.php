<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){

        $users = User::get();
        return view('users.index', compact('users'));
    }

    public function show($idu){

        $user = User::where('id', $idu)
        ->firstOrFail();
        return view('users.show',compact('user'));
    }
}
