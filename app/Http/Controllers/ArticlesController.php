<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Categorie;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{


    public function index(){

        $articles = Article::with("author")
        ->with("categories")
        ->get();
        return view('articles.index', compact('articles'));
    }

    public function show($id){

        $article = Article::where('id', $id)
        ->firstOrFail();
        return view('articles.show',compact('article'));
    }



    public function create()
    {
        $article = new Article();
        $categories = Categorie::pluck('name','id');
        return view('blog.crud.create', compact('article','categories'));
    }

    public function store(Request $request)
    {
        
    }

    public function edit(Article $article)
    {
        return view('');
    }

    public function update($id)
    {

    }


}
