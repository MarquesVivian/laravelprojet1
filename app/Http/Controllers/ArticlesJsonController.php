<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticlesJsonController extends Controller
{
    public array $articles = [];

    /**
     * Constructeur
     * On stocke tous nos articles dans un attribut articles
     */
    public function __construct()
    {
        $articlesJsonFile = file_get_contents(resource_path('data/articles.json'));

        $this->articles = json_decode($articlesJsonFile)->articles;
    }

    /**
     * Liste des tous nos articles
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $articles = $this->articles;
        return view('articles.index', compact('articles'));
    }

    /**
     * Vue d'un article
     *
     * @param $iteration
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($iteration)
    {
        if( $iteration < 1 || $iteration > count($this->articles) ) {
            abort(404);
        }

        $article = $this->articles[$iteration - 1];
        return view('articles.show', compact('article'));
    }

    public function author($name)
    {

    }
}
