<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Categorie;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {

        $categories = Categorie::withcount('article')
        ->orderby('name','ASC')
        ->get();

        return view('blog.categories', compact('categories'));
    }

    public function show($id){
    
        $categorie = Categorie::with('article')
        ->where('id', $id)
        ->firstOrFail();

        return view('blog.category', compact('categorie'));  
    }
}
