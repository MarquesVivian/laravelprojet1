<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public $timestamps = false;

    public function article()
    {
        return $this->belongsToMany(Article::class,
        "articles_categories",
        "categories_id",
        "article_id"
    );
    }
}
