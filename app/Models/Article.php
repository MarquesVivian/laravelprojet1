<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable =['title',
    'author_id',
    'content',
    'published_at',
    'image_url'];

    public $dates = ['published_at'];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Categorie::class,
        "articles_categories",
        'article_id',
        'categories_id');
    }
}
