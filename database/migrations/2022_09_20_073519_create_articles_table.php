<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('title')->unique();
            $table->unsignedBigInteger('author_id')->index();
            $table->text('content');
            $table->dateTime('published_at');
            $table->string('image_url')->nullable();

            $table->timestamps();
        });

        Schema::table('articles', function(Blueprint $table){
            $table->foreign('author_id')->references('id')->on('users')->cascadeOnDelete();
        });
        
        Schema::create('categories', function(Blueprint $table){
            $table->id();
            $table->string('name');

        });

        Schema::create('articles_categories', function(Blueprint $table){
            $table->id();
            $table->unsignedBigInteger('article_id')->index();
            $table->unsignedBigInteger('categories_id')->index();
        });

        Schema::table('articles_categories', function(Blueprint $table){
            $table->foreign('article_id')->references('id')->on('articles')->cascadeOnDelete();
            $table->foreign('categories_id')->references('id')->on('categories')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('articles_categories', function(Blueprint $table){
            $table->dropForeign(['article_id']);
            /*$table->dropForeign('articles_categories_category_id_foreign'); // parreil*/
        });

        Schema::table('articles', function(Blueprint $table){
            $table->dropForeign(['author_id']);
        });

        Schema::dropIfExists('articles');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('articles_categories');


    }
};
