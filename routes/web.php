<?php

use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \App\Http\Controllers\DemoController;
use \App\Http\Controllers\CategoriesController;
use App\Http\Controllers\UserController;

//Route::get('/', [DemoController::class, 'index'])->name('home');

//Route::get('/users/{name}', [DemoController::class, 'name'])->whereAlphaNumeric('name')->name('name');

//Route::get('articles', [\App\Http\Controllers\ArticlesJsonController::class, 'index'])->name('articles.index');
//Route::get('articles/{iteration}', [\App\Http\Controllers\ArticlesJsonController::class, 'show'])->whereNumber('iteration')->name('articles.show');

Route::prefix('blog')->group(function(){
    Route::get('/categories',
    [CategoriesController::class,'index'])
    ->name('blog.categories.index');

    Route::get('/categories/{category}',
    [CategoriesController::class,'show'])
    ->name('blog.categories.show');

    Route::get('/auteurs',
    [UserController::class,'index'])
    ->name('blog.auteurs.index');

    Route::get('/auteurs/{idu}',
    [UserController::class,'show'])
    ->name('blog.auteur.show');

    Route::get('/',
    [ArticlesController::class,'index'])
    ->name('blog.articles.index');

    Route::get('/{id}',
    [ArticlesController::class,'show'])
    ->name('blog.articles.show');


});
