<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \App\Http\Controllers\DemoController;
use App\Http\Controllers\ArticlesController;

Route::get('/', [DemoController::class, 'index'])->name('home');

Route::get('/users/{name}', [DemoController::class, 'name'])->whereAlphaNumeric('name')->name('name');



Route::get('articles', [ArticlesController::class, 'index'])->name("Varticles");


/*Route avec View*/
Route::get('welcome', function () {
    return view('welcome');
})->name("View");